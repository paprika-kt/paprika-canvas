plugins {
    id("com.android.library")
    kotlin("multiplatform")
}


val lwjglVersion = "3.2.1"

val lwjglNatives = when (org.gradle.internal.os.OperatingSystem.current()) {
    org.gradle.internal.os.OperatingSystem.WINDOWS -> "natives-windows"
    org.gradle.internal.os.OperatingSystem.LINUX -> "natives-linux"
    org.gradle.internal.os.OperatingSystem.MAC_OS -> "natives-macos"
    else -> error("OS not recognized")
}

//
android {
    compileSdkVersion(28)
    defaultConfig {
        minSdkVersion(15)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
    }
}

version = "0.0.1"

repositories {
    jcenter()
    google()
    mavenCentral()
}

kotlin {
    jvm()
    js()
    iosX64("ios") {
        binaries {
            framework()
        }
    }

    android("android")

    iosArm64("iosDevice") {
        binaries {
            framework()
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(project(":disposable"))
                implementation(project(":paprika-koml"))
            }
        }
        val commonTest by getting {
            dependencies {
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.lwjgl:lwjgl-glfw:$lwjglVersion")
                implementation("org.lwjgl:lwjgl-glfw:$lwjglVersion:$lwjglNatives")
                implementation(project(":paprika-koml"))
            }
        }


        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }

        val jsMain by getting {
            dependencies {
                implementation(kotlin("stdlib-js"))
            }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }


        val androidMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
                implementation(project(":paprika-koml"))
            }
        }

        val androidTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
    }
}