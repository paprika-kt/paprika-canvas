package paprika

interface Capabilities {
    val supported: Boolean
}