package paprika

import ca.jsbr.disposable.Disposable


@Suppress("unused")
open class Signal<T> {
    private var _listeners: ArrayList<(T) -> Unit> = arrayListOf()

    fun add(func: (T) -> Unit): Disposable {
        _listeners.add(func)
        return Disposable.create {
            _listeners.remove(func)
        }
    }

    fun dispatch(value: T) {
        for (l in _listeners.toTypedArray()) // find a better solution warning to async
            l(value)
    }

    fun dispose() {
        _listeners.clear()
    }

    operator fun invoke(func: (T) -> Unit) = add(func)
    operator fun invoke(value: T) = dispatch(value)
}

fun <T> Signal<T>.once(func: (T) -> Unit): Disposable {
    var disposable: Disposable? = null
    disposable = this.add {
        func(it)
        disposable!!.dispose()
    }
    return disposable
}