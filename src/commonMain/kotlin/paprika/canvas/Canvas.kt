package paprika.canvas

import paprika.koml.geom.Sizef

interface Canvas {
    val width: Int
    val height: Int
    val pixelSize: Sizef
    fun update(action: (delta: Float) -> Unit)
}