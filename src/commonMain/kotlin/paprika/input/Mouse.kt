package paprika.input

import paprika.koml.geom.Vector2
import paprika.Signal

interface Mouse {
    val pointer: Vector2 // review use vector float or double

    val onMove: Signal<Vector2>
    val onDown: Signal<MouseButton>
    val onUp: Signal<MouseButton>

    fun hasButtonDown(button: Int): Boolean
}

data class MouseButton(val button: Int, val pointer: Pointer)