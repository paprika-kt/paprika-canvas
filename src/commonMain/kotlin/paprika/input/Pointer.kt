package paprika.input

import paprika.koml.geom.Vector2
import paprika.Signal


interface Pointer {
    val pointer: PointerType
    val index: Int
    val position: Vector2
    val state: PointerState

    //option2
    val move: Signal<PointerData>
    val up: Signal<PointerData>
    val down: Signal<PointerData>
}

interface PointerData {
    val type: PointerType
    val index: Int
    val position: Vector2
    val state: PointerState
}


enum class PointerState {
    DOWN,
    UP,
    MOVE
}

enum class PointerType {
    MOUSE,
    TOUCH,
}