package paprika.canvas

import paprika.koml.geom.Sizef
import kotlinx.cinterop.ExportObjCClass
import platform.EAGL.EAGLContext
import platform.Foundation.NSCoder
import platform.GLKit.GLKView
import platform.GLKit.GLKViewController

enum class EAGLRenderingAPI(val rawValue: ULong) {
    OPENGL_ES1(0u),
    OPENGL_ES2(1u),
    OPENGL_ES3(2u)
}

@ExportObjCClass(name = "PaprikaView")
class PaprikaView : GLKViewController, platform.GLKit.GLKViewControllerDelegateProtocol { // TODO

    val canvas = IOSCanvas()

    @OverrideInit
    constructor(coder: NSCoder) : super(coder)

    fun setupGL() {

        val context = EAGLContext(EAGLRenderingAPI.OPENGL_ES2.rawValue)
        // 2
        EAGLContext.setCurrentContext(context)
        val view = this.view as? GLKView ?: error("the current view is not GLKView")
        view.context = context
        delegate = this

    }

    override fun glkViewControllerUpdate(controller: GLKViewController) {
        canvas.trriggerUpdate(1 / 60f)
    }

    override fun viewDidAppear(animated: Boolean) {
        super.viewDidAppear(animated)
        val v  = view as GLKView
        canvas.height = v.drawableHeight.toInt()
        canvas.width = v.drawableWidth.toInt()
    }

    override fun viewDidLoad() {
        super.viewDidLoad()
        setupGL()
    }
}

class IOSCanvas : Canvas {

    var updater: (delta: Float) -> Unit = {}

    override var width: Int = 1792
    override var height: Int = 828
    override var pixelSize = Sizef(1f,1f)

    override fun update(action: (delta: Float) -> Unit) {
        updater = action
    }

    fun trriggerUpdate(delta: Float) {
        updater
    }
}