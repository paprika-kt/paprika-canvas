package paprika.input

import kotlinx.cinterop.CValue
import platform.CoreGraphics.CGPoint
import platform.UIKit.UITouch

interface MouseHandler {
    fun trigger(touch: UITouch, x: Int, y: Int): Boolean
}