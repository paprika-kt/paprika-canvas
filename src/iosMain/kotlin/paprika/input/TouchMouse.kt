package paprika.input

//import android.view.MotionEvent
import paprika.koml.geom.MutableVector2
import paprika.koml.geom.Vector2
import paprika.Signal
import platform.UIKit.UITouch
import platform.UIKit.UITouchPhase

class TouchMouse : MouseHandler, Mouse {

    override val pointer = MutableVector2(0, 0)
    override val onMove: Signal<Vector2> = Signal()
    override val onDown: Signal<MouseButton>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    override val onUp: Signal<MouseButton>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    val buttons: BooleanArray = booleanArrayOf(false, false, false, false, false, false, false, false)

    override fun hasButtonDown(button: Int): Boolean = buttons[button]

    override fun trigger(touch: UITouch, x: Int, y: Int): Boolean {
        pointer.x = x
        pointer.y = y
        println("touch: ${pointer.x}x${pointer.y}")
        val down = touch.phase == UITouchPhase.UITouchPhaseBegan
        buttons[0] = down
        return down
    }
}