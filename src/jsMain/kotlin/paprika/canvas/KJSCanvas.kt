package paprika.canvas

import paprika.koml.geom.Sizef
import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document
import kotlin.browser.window
import kotlin.js.Date


class KJSCanvas(
    val canvas: HTMLCanvasElement,
    private val resolutionFactor: Float = 2f
) : Canvas {

    override var width: Int = canvas.clientWidth
    override var height: Int = canvas.clientHeight
    private var last = 0.0
    override var pixelSize: Sizef = Sizef(1f, 1f)
        private set

    init {
        onResize()
        document.body!!.addEventListener("resize", {
            onResize()
        }, null)
        canvas.addEventListener("resize", {
            onResize()
        }, null)
    }

    private fun onResize() {
        console.log("1) ${width}x${height} ${canvas.clientWidth}x${canvas.clientHeight} ${canvas.width}x${canvas.height}")

        pixelSize.width = resolutionFactor
        pixelSize.height = resolutionFactor
        width = (canvas.clientWidth * resolutionFactor).toInt()
        height = (canvas.clientHeight * resolutionFactor).toInt()
        canvas.width = (canvas.clientWidth * resolutionFactor).toInt()
        canvas.height = (canvas.clientHeight * resolutionFactor).toInt()
        console.log("2) ${width}x${height} ${canvas.clientWidth}x${canvas.clientHeight} ${canvas.width}x${canvas.height}")
    }


    override fun update(action: (delta: Float) -> Unit) {
        window.requestAnimationFrame {
            val now = Date.now()
            action(((now - last) / 1000).toFloat())
            last = now
            update(action)
        }
    }
}