package paprika.input

import paprika.koml.geom.MutableVector2
import paprika.koml.geom.Vector2
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.events.MouseEvent
import paprika.Capabilities
import paprika.Signal
import paprika.canvas.Canvas

class KJSMouse(canvas: HTMLCanvasElement, kjsCanvas: Canvas) : Mouse, Capabilities {
    override val supported: Boolean = true

    override val pointer = MutableVector2(0, 0)
    private val buttons: BooleanArray = booleanArrayOf(false, false, false, false, false, false, false, false)

    override val onMove: Signal<Vector2> = Signal()
    override val onDown: Signal<MouseButton>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    override val onUp: Signal<MouseButton>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    private val rect = canvas.getBoundingClientRect()

    init {
        canvas.addEventListener("mousemove", { evt ->
            evt as MouseEvent
            pointer.x = ((evt.clientX - rect.left) * kjsCanvas.pixelSize.width).toInt()
            pointer.y = ((evt.clientY - rect.top) * kjsCanvas.pixelSize.height).toInt()
            onMove.dispatch(pointer)
        })
        canvas.addEventListener("mousedown", {
            println("mousedown")
            buttons[0] = true
        })
        canvas.addEventListener("mouseup", {
            println("mouseup")
            buttons[0] = false
        })
    }

    override fun hasButtonDown(button: Int): Boolean = buttons[button]
}