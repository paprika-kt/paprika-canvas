package paprika.canvas

import paprika.koml.geom.Size
import paprika.koml.geom.Sizef
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback
import org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.glfw.GLFWKeyCallback
import org.lwjgl.system.MemoryUtil
import paprika.input.MouseHandler


class GLFWWindow(override var width: Int, override var height: Int, val mouse: MouseHandler) : Canvas {

    private var errorCallback: GLFWErrorCallback? = null
    private var keyCallback: GLFWKeyCallback? = null
    val size: Size = Size(width, height)

    override var pixelSize = Sizef(1f, 1f)
    private var window: Long? = null

    fun init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        errorCallback = GLFW.glfwSetErrorCallback(GLFWErrorCallback.createPrint(System.err))

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if (!GLFW.glfwInit()) {
            throw IllegalStateException("Unable to initialize GLFW")
        }

        // Configure our window
        GLFW.glfwDefaultWindowHints()
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 2)
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 1)

//        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE)
//        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GLFW.GLFW_TRUE)

        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE)
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE)
        // Create the window
        window = GLFW.glfwCreateWindow(width, height, "Hello World!", MemoryUtil.NULL, MemoryUtil.NULL)
        if (window == MemoryUtil.NULL) {
            throw RuntimeException("Failed to create the GLFW window")
        }

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        keyCallback = GLFW.glfwSetKeyCallback(window!!, object : GLFWKeyCallback() {
            override fun invoke(window: Long,
                                key: Int,
                                scancode: Int,
                                action: Int,
                                mods: Int) {

                if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_RELEASE) {
                    GLFW.glfwSetWindowShouldClose(window, true)
                }

            }
        })

        // Get the resolution of the primary monitor
        val vidmode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor())

        // Center our window
        GLFW.glfwSetWindowPos(
                window!!,
                (vidmode!!.width() - width) / 2,
                (vidmode.height() - height) / 2
        )

        glfwSetCursorPosCallback(window!!) { _, x, y ->
            mouse.invokeMouseMove((x * pixelSize.width).toInt(), (y * pixelSize.height).toInt())
        }
        glfwSetMouseButtonCallback(window!!, mouse::invokeButtonStateChange)
        GLFW.glfwSetFramebufferSizeCallback(window!!) { _, w, h ->
            width = w
            height = h
            size.set(w, h)
        }

        // Make the OpenGL context current

        GLFW.glfwMakeContextCurrent(window!!)
        // v-sync 0 || 1
        GLFW.glfwSwapInterval(1)

        // Make the window visible
        GLFW.glfwShowWindow(window!!)

        val w = BufferUtils.createIntBuffer(1)
        val h = BufferUtils.createIntBuffer(1)

        GLFW.glfwGetWindowSize(window!!, w, h)
        val width1 = w.get(0)
        val height1 = h.get(0)

        GLFW.glfwGetFramebufferSize(window!!, w, h) // solve issue with retina display
        width = w.get(0)
        height = h.get(0)
        size.set(width, height)
        pixelSize = Sizef(width / width1.toFloat(), height / height1.toFloat())

    }

    override fun update(action: (Float) -> Unit) {

        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
//        GL.createCapabilities()

        // Set the clear color
//        GL11.glClearColor(1.0f, 0.5f, 0.0f, 1.0f)

        var time = System.currentTimeMillis()
        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!GLFW.glfwWindowShouldClose(window!!)) {

            // Clear the framebuffer
//            glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
            val current = System.currentTimeMillis()
            action((current - time) / 1000f)
            time = current
            // Swap the color buffers
            GLFW.glfwSwapBuffers(window!!)

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            GLFW.glfwPollEvents()
        }
//        throw RuntimeException("Terminate")
    }

    fun close() {
        // Terminate GLFW
        GLFW.glfwTerminate()
        errorCallback?.close()
    }

}