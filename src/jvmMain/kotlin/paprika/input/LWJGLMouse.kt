package paprika.input

import paprika.koml.geom.MutableVector2
import paprika.koml.geom.Vector2
import org.lwjgl.glfw.GLFW
import paprika.Capabilities
import paprika.Signal

class LWJGLMouse : MouseHandler, Mouse, Capabilities {
    override val supported: Boolean = true
    override val pointer = MutableVector2(0, 0)
    override val onMove: Signal<Vector2> = Signal()
    override val onDown: Signal<MouseButton>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    override val onUp: Signal<MouseButton>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    val buttons: BooleanArray = booleanArrayOf(false, false, false, false, false, false, false, false)

    override fun hasButtonDown(button: Int): Boolean = buttons[button]

    override fun invokeMouseMove(xpos: Int, ypos: Int) {
        pointer.x = xpos//(xpos * canvas.pixelSize.width).toInt()
        pointer.y = ypos//(ypos * canvas.pixelSize.height).toInt()
        onMove.dispatch(pointer)
    }

    override fun invokeButtonStateChange(window: Long, button: Int, action: Int, mods: Int) {
        val isDown = action == GLFW.GLFW_PRESS
        buttons[button] = isDown
    }

}