package paprika.input

interface MouseHandler {
    fun invokeButtonStateChange(window: Long, button: Int, action: Int, mods: Int)
    fun invokeMouseMove(xpos: Int, ypos: Int)
}