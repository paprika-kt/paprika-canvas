package paprika.canvas

import android.app.Activity
import android.content.Context
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.SurfaceView
import paprika.Signal
import paprika.input.MouseHandler
import paprika.koml.geom.Sizef
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

// TODO to review
abstract class PaprikaActivity : Activity() {

    abstract val glView: PaprikaGLSurfaceView

    public override fun onCreate(savedInstanceState: Bundle?) {
        Log.i("Paprika", "onCreate")
        super.onCreate(savedInstanceState)

        glView.renderer.initied.add { init() }
        setContentView(glView)
    }

    abstract fun init()
}


class PaprikaGLSurfaceView(context: Context, private val mouseHandler: MouseHandler) :
    GLSurfaceView(context) {

    val renderer: MyGLRenderer
    val canvas: Canvas

    init {
        setEGLContextClientVersion(2)

        renderer = MyGLRenderer(this, context)
        canvas = renderer

        setRenderer(renderer)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        super.onTouchEvent(event)
        return mouseHandler.trigger(event!!)
    }


}

class MyGLRenderer(
    val m: SurfaceView,
    val context: Context
) : GLSurfaceView.Renderer, Canvas {

    override val pixelSize: Sizef = Sizef(1f, 1f)
    override val width: Int
        get() = m.width

    override val height: Int
        get() = m.height

    private var updater: (delta: Float) -> Unit = {}
    private var time = System.currentTimeMillis()
    val initied = Signal<Boolean>()

    init {

    }

    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {
        initied.dispatch(true)
    }

    override fun onDrawFrame(unused: GL10) {
        val current = System.currentTimeMillis()
        updater((current - time) / 1000f)
        time = current
    }

    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)
    }

    override fun update(action: (delta: Float) -> Unit) {
        updater = action
    }
}