package paprika.input

import android.view.MotionEvent

interface MouseHandler {
    fun trigger(event: MotionEvent): Boolean
}