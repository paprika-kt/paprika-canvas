package paprika.input

import android.view.MotionEvent
import paprika.koml.geom.MutableVector2
import paprika.Signal
import paprika.koml.geom.Vector2

class TouchMouse : MouseHandler, Mouse {

    override val pointer = MutableVector2(0, 0)
    override val onMove: Signal<Vector2> = Signal()
    override val onDown: Signal<MouseButton>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    override val onUp: Signal<MouseButton>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    val buttons: BooleanArray = booleanArrayOf(false, false, false, false, false, false, false, false)

    override fun hasButtonDown(button: Int): Boolean = buttons[button]

    override fun trigger(event: MotionEvent): Boolean {
        pointer.x = event.x.toInt()
        pointer.y = event.y.toInt()
        println("${pointer.x}x${pointer.y}")
        val down = event.pointerCount > 0 && event.action != MotionEvent.ACTION_UP
        buttons[event.actionIndex] = down
        return down
    }
}